﻿using System;
using BotatHWR.Bot;

namespace BotatHWR
{
    class Program
    {
        static void Main(string[] args)
        {
            Arm arm = new Arm();

            Console.WriteLine("up - arm moves up");
            Console.WriteLine("down - arm moves down");
            Console.WriteLine("join - hand closes");
            Console.WriteLine("divide - hand opens");
            Console.WriteLine("exit - finishes the program");

            string consoleInput = "";

            while (consoleInput != "exit")
            {
                consoleInput = Console.ReadLine();

                switch (consoleInput)
                {
                    case "up":
                        arm.up();
                        break;
                    case "down":
                        arm.down();
                        break;
                    case "join":
                        arm.join();
                        break;
                    case "divide":
                        arm.divide();
                        break;
                }
            }
        }
    }
}